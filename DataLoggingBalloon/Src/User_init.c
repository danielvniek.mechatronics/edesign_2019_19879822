extern volatile float longB;
extern volatile float altB;
extern volatile int burnCount;
extern volatile float longB;
extern volatile float altB;
extern int presTotal;
extern int tempTotal;
extern int humTotal;
float GPSconvert(char GPS[], int L){
	float degrees = 0.0;

	degrees = 10*(GPS[L-7]-48) + (GPS[L-6]-48) +0.1*(GPS[L-4]-48) +0.01*(GPS[L-3]-48)+0.001*(GPS[L-2]-48)+0.0001*(GPS[L-1]-48);

	degrees = (GPS[L-8]-48) + degrees/60;
	if (L>8){
		degrees = degrees + 10*(GPS[L-9]-48);
	}
	if (L>9){
				degrees = degrees + 100*(GPS[L-10]-48);
				longB = degrees;

			}
	return degrees;

}
#include <math.h>
void GPSDegString(char outputString[], int L, float val, char sign){
	int latInt = 0;
	int latDec = 0;
	int i;
	latInt = (int)floor(val);
	latDec = (int)floor(1000000*(val-floor(val)));
	for (i = 1;i<7;i++){
		outputString[L-i] = 48+(latDec%10);
		latDec = latDec/10;
	}
	outputString[L-7]='.';
	for (i = 8;latInt>0;i++){
		outputString[L-i] = 48+(latInt%10);
		latInt = latInt/10;
	}
	outputString[L-i] = sign;

}

int hexToDecimal(char hexVal)
{
    int dec_val = 0;

        // if character lies in '0'-'9', converting
        // it to integral 0-9 by subtracting 48 from
        // ASCII value.
        if (hexVal>='0' && hexVal<='9')
        {
            dec_val = hexVal - 48;
        }

        // if character lies in 'A'-'F' , converting
        // it to integral 10 - 15 by subtracting 55
        // from ASCII value
        else if (hexVal>='A' && hexVal<='F')
        {
            dec_val = (hexVal - 55);
        }
    return dec_val;
}

void altConvert(char alt[], int L, char altOut[]){
	int pointPos;
	int zeroCheck;
	int i;
	float altVal;
	int alt10Int;
	for(i = 0; i<L; i++){
		if(alt[i] == '.'){
			pointPos = i;
		}
	}
		altVal = 0;
	for(i = pointPos +1;i<L;i++){
		altVal = altVal + pow(0.1, i - (pointPos))*(alt[i]-48);
	}
	for(i = pointPos - 1; i>=0; i--){
		altVal = altVal + pow(10, pointPos-1 - i)*(alt[i]-48);
	}
	altB = altVal;
	altVal = altVal*10;
	alt10Int = round(altVal);
	zeroCheck = 0;
	for(i = 6; i>=0;i--){

	if(i == 5){
		altOut[i] = '.';
	}
	else{

		if(alt10Int>0){
			altOut[i] = alt10Int%10+48;
			alt10Int = alt10Int/10;
			zeroCheck = 1;
		}else{
			altOut[i] = ' ';
		}
		if (zeroCheck == 0){
			altOut[4] = '0';
			altOut[6] = '0';
		}

	}

	}

}

void stringCombine(char str[], char stdnumber[],char timeC[], char timeofday[],char tmp[],char hum[],char prs[],char acc1[],char acc2[],char acc3[], char lat[], char longtd[], char alt[], char cur[],char vol[]){
	int i=0;
	int j=0;
	str[0] = '$';
	j = 0;
	for (i = 1;i<9;i++){
		str[i] = stdnumber[j];
		j++;
	}
	str[9] = ',';
	j = 0;
	for(i = 10;i<15;i++){
		str[i] = timeC[j];
		j++;
	}
	str[15] = ',';
	j = 0;
	for(i = 16;i<24;i++){
		str[i] = timeofday[j];
		j++;
	}
	str[24] = ',';
	j = 0;
	for(i = 25;i<28;i++){
		str[i] = tmp[j];
		j++;
	}
	str[28] = ',';
	j = 0;
	for(i = 29;i<32;i++){
		str[i] = hum[j];
		j++;
	}
	str[32] = ',';
	j = 0;
	for(i = 33;i<36;i++){
		str[i] = prs[j];
		j++;
	}
	str[36] = ',';
	j = 0;
	for(i = 37;i<41;i++){
		str[i] = acc1[j];
		j++;
	}
	str[41] = ',';
	j = 0;
	for(i = 42;i<46;i++){
		str[i] = acc2[j];
		j++;
	}
	str[46] = ',';
	j = 0;
	for(i = 47;i<51;i++){
		str[i] = acc3[j];
		j++;
	}
	str[51] = ',';
	j = 0;
	for(i = 52;i<62;i++){
		str[i] = lat[j];
		j++;
	}
	str[62] = ',';
	j = 0;
	for(i = 63;i<74;i++){
		str[i] = longtd[j];
		j++;
	}
	str[74] = ',';
	j = 0;
	for(i = 75;i<82;i++){
		str[i] = alt[j];
		j++;
	}
	str[82] = ',';
	j = 0;
	for(i = 83;i<86;i++){
		str[i] = cur[j];
		j++;
	}
	str[86] = ',';
	j = 0;
	for(i = 87;i<90;i++){
		str[i] = vol[j];
		j++;
	}

	str[90] = '\n';
}

extern volatile void curConvert(char curArray[], float currentAvg){
	int current;
	int i;
	if (currentAvg <1000.0){
		current=round(currentAvg);
		curArray[2] = 48 + current%10;
		current=current/10;
		i = 1;
		for(i = 1;i>=0;i--){
			if (current>0){
				curArray[i] = 48 + current%10;
				current=current/10;
			}else{
				curArray[i] = ' ';
			}
		}


	}else{
		curArray[2] = '*';
		curArray[1] = '*';
		curArray[1] = '*';
	}

}
extern volatile void volConvert(char volArray[], float voltageAvg){
	int voltage;
	if (voltageAvg <10.0){
			voltage = round(10*voltageAvg);
			volArray[2] = 48 + voltage%10;
			voltage = voltage/10;
			volArray[0] = 48 + voltage;
			volArray[1] = '.';
	}else{
		volArray[2] = '*';
		volArray[1] = '*';
		volArray[0] = '*';
	}


}
#include <main.h>
void LCDoutput(int RS,int RW,int DB7,int DB6,int DB5,int DB4);
volatile void LCDinit(){
	//init1
	HAL_Delay(19);
	LCDoutput(0,0,0,0,1,1);
	//init2
	HAL_Delay(4);
	LCDoutput(0,0,0,0,1,1);
	//init3
	LCDoutput(0,0,0,0,1,1);

	//choose 4bit mode
	LCDoutput(0,0,0,0,1,0);
	//repeat 4bit mode selection
	LCDoutput(0,0,0,0,1,0);
	//choose 2 lines and 5x7 dots
	LCDoutput(0,0,1,0,0,0);

	//Display ON/OFF PART 1
	LCDoutput(0,0,0,0,0,0);
	//Display ON/OFF PART 2
	LCDoutput(0,0,1,1,1,0);

	//Clear Display Part 1
	LCDoutput(0,0,0,0,0,0);
	//Clear Display Part 2
	LCDoutput(0,0,0,0,0,1);

	//Entry Mode Set Part1
	LCDoutput(0,0,0,0,0,0);
	//Entry Mode Set Part2
	LCDoutput(0,0,0,1,1,0);
}

void LCDoutput(int RS,int RW,int DB7,int DB6,int DB5,int DB4){
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, RS);
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, RW);
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, 1);//en
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, DB7);//db7
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, DB6);//db6
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, DB5);//db5
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, DB4);//db4
	  HAL_Delay(1);
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, 0);//en
	  HAL_Delay(1);

}

volatile void updateLCD(float altitude, int burn, int temperature){
	LCDoutput(0,0,0,0,0,0);
	LCDoutput(0,0,0,0,0,1);
	int i;
	int divis;
	int altLen;
	altLen= 1;
	int altInt;
	int intOut;
	altInt = round(altitude);
	if (altInt > 9){
		altLen = 2;
	}
	if (altInt >= 99){
		altLen = 3;
	}
	if (altInt >= 999){
		altLen = 4;
	}
	if (altInt >= 9999){
		altLen = 5;
	}
	for(i=1;i<=altLen;i++){
		divis = pow(10,altLen-i);
		intOut = altInt/divis;
		altInt = altInt%divis;
		LCDintToBits(intOut);
	}
	LCDoutput(1,0,0,1,1,0); //'m'
	LCDoutput(1,0,1,1,0,1);

	LCDoutput(0,0,1,1,0,0);//move to burn adress
	LCDoutput(0,0,0,0,0,1);
	if (burn ==1){
		LCDoutput(1,0,0,1,0,0); //'b'
		LCDoutput(1,0,0,0,1,0);
	}
	int tempLen;
	tempLen = 1;
	int tempSign;
	tempSign = 0;

	if(temperature<0){
		tempSign = 1;
		temperature = abs(temperature);
	}
	if(temperature>9){
		tempLen = 2;
	}
	int tempStartAddress;
	tempStartAddress = 71 - (tempLen+tempSign); //CONTINUE here
	if(tempStartAddress==70){
		LCDoutput(0,0,1,1,0,0);
		LCDoutput(0,0,0,1,1,0);
	}
	if(tempStartAddress==69){
		LCDoutput(0,0,1,1,0,0);
		LCDoutput(0,0,0,1,0,1);
	}
	if(tempStartAddress==68){
		LCDoutput(0,0,1,1,0,0);
		LCDoutput(0,0,0,1,0,0);
	}


	if(tempSign==1){
		LCDoutput(1,0,0,0,1,0); //write '-' if necessary
		LCDoutput(1,0,1,1,0,1);
	}
	for(i=1;i<=tempLen;i++){
			divis = pow(10,tempLen-i);
			intOut = temperature/divis;
			temperature = temperature%divis;
			LCDintToBits(intOut);
		}
		LCDoutput(1,0,0,1,0,0); //'C'
		LCDoutput(1,0,0,0,1,1);



}

void LCDintToBits(int intOut){
	int DB3;
	int DB2;
	int DB1;
	int DB0;
	LCDoutput(1,0,0,0,1,1);
	DB3=0;
	DB2=0;
	DB1=0;
	DB0=0;

	if(intOut>7){
		DB3 = 1;
	}
	if((intOut==4)||(intOut==5)||(intOut==6)||(intOut==7)){
		DB2 = 1;
	}
	if((intOut==2)||(intOut==3)||(intOut==6)||(intOut==7)){
		DB1 = 1;
	}
	if(intOut%2 == 1){
		DB0 = 1;
	}
	LCDoutput(1,0,DB3,DB2,DB1,DB0);

	}
#include "bme280.h"
  uint8_t settings_sel;
  struct bme280_dev dev;
  int8_t rslt = BME280_OK;
  struct bme280_data comp_data;
void user_delay_ms(uint32_t period);
int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);
int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);
void PresTempHumToChar(char pth[], int avg);
void BMEinit(){
    dev.dev_id = BME280_I2C_ADDR_PRIM;
    dev.intf = BME280_I2C_INTF;
    dev.read = user_i2c_read;
    dev.write = user_i2c_write;
    dev.delay_ms = user_delay_ms;

    rslt = bme280_init(&dev);
    dev.settings.osr_h = BME280_OVERSAMPLING_1X;
		dev.settings.osr_p = BME280_OVERSAMPLING_16X;
		dev.settings.osr_t = BME280_OVERSAMPLING_2X;
		dev.settings.filter = BME280_FILTER_COEFF_16;
		dev.settings.standby_time = BME280_STANDBY_TIME_0_5_MS;

		settings_sel = BME280_OSR_PRESS_SEL;
		settings_sel |= BME280_OSR_TEMP_SEL;
		settings_sel |= BME280_OSR_HUM_SEL;
		settings_sel |= BME280_STANDBY_SEL;
		settings_sel |= BME280_FILTER_SEL;
		rslt = bme280_set_sensor_settings(settings_sel, &dev);
		rslt = bme280_set_sensor_mode(BME280_NORMAL_MODE, &dev);
}

void user_delay_ms(uint32_t period)
{
	HAL_Delay(period);
}
I2C_HandleTypeDef hi2c1;
int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
    int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */

    rslt = HAL_I2C_Mem_Read(&hi2c1, 0xec, reg_addr, 1, reg_data, len, 200);



    /*
     * The parameter dev_id can be used as a variable to store the I2C address of the device
     */

    /*
     * Data on the bus should be like
     * |------------+---------------------|
     * | I2C action | Data                |
     * |------------+---------------------|
     * | Start      | -                   |
     * | Write      | (reg_addr)          |
     * | Stop       | -                   |
     * | Start      | -                   |
     * | Read       | (reg_data[0])       |
     * | Read       | (....)              |
     * | Read       | (reg_data[len - 1]) |
     * | Stop       | -                   |
     * |------------+---------------------|
     */

    return rslt;
}

int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
    int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */
    rslt = HAL_I2C_Mem_Write(&hi2c1, 0xec, reg_addr, 1, reg_data, len, 200);
    /*
     * The parameter dev_id can be used as a variable to store the I2C address of the device
     */

    /*
     * Data on the bus should be like
     * |------------+---------------------|
     * | I2C action | Data                |
     * |------------+---------------------|
     * | Start      | -                   |
     * | Write      | (reg_addr)          |
     * | Write      | (reg_data[0])       |
     * | Write      | (....)              |
     * | Write      | (reg_data[len - 1]) |
     * | Stop       | -                   |
     * |------------+---------------------|
     */

    return rslt;
}

void getPresTempHum(){
	//dev.delay_ms(70);
    rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, &dev);
    presTotal = presTotal + comp_data.pressure/100000;
    tempTotal = tempTotal + comp_data.temperature/100;
    humTotal = humTotal + comp_data.humidity/1024;
}
void convertPresTempHum(char prs[], char tmp[], char hum[], int avgPres, int avgTemp, int avgHum){
	PresTempHumToChar(prs, avgPres);
	PresTempHumToChar(tmp, avgTemp);
	PresTempHumToChar(hum, avgHum);



}
void PresTempHumToChar(char pth[], int avg){
	int sign, i;
	sign = 0;
	if (avg < 0){
		sign = 1;
	}
	avg = abs(avg);
	pth[2] = avg%10+48;
	avg = avg/10;
		if(sign==1){
		if(avg>9){
			pth[0]='-';
			pth[1] = avg%10+48;
		}else{
			pth[1] = '-';
			pth[0] = ' ';
		}
		}else{
			for (i=1;i>=0;i--){
					if(avg >0){
					pth[i]=avg%10 +48;
					avg = avg/10;
					}else{
						pth[i] = ' ';
					}
		}
	}

}

